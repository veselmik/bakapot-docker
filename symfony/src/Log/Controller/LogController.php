<?php

declare(strict_types=1);

namespace App\Log\Controller;

use App\Log\Service\LogManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use function is_array;

/** @IsGranted("ROLE_LOGGER") */
class LogController extends AbstractController
{
    #[Route('/auth/log', name: 'auth.log', methods: ['POST'])]
    public function saveLog(Request $request, LogManager $logger) : JsonResponse
    {
        $logData = $request->get('logData');
        if ($logData === null || ! is_array($logData)) {
            $response = ['success' => false, 'message' => 'No log data found'];
        } else {
            $response = $logger->saveData($logData);
        }

        return new JsonResponse(
            $response
        );
    }
}
