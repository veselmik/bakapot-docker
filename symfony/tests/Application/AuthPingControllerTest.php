<?php

declare(strict_types=1);

namespace App\Tests\Application;

use App\DataFixtures\UserFixtures;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

use function json_decode;
use function json_encode;

class AuthPingControllerTest extends WebTestCase
{
    private const ENDPOINT = '/auth/ping';

    public function testNonAuthenticatedUserPing() : void
    {
        $browser = static::createClient();
        $browser->request(Request::METHOD_GET, self::ENDPOINT);
        $response = (string) $browser->getResponse()->getContent();
        $responseCode = $browser->getResponse()->getStatusCode();

        $this->assertJson($response);
        $this->assertSame(401, $responseCode);
    }

    public function testAuthenticatedUserPing() : void
    {
        $testingUser = ['email' => UserFixtures::TEST_USER_EMAIL, 'password' => UserFixtures::TEST_USER_PASSWORD];
        /** Get token */
        $browser = static::createClient();
        $browser->request(
            Request::METHOD_POST,
            '/login_check',
            server: ['CONTENT_TYPE' => 'application/json'],
            content: (string) json_encode($testingUser)
        );
        $response = (string) $browser->getResponse()->getContent();
        $this->assertJson($response);
        $response = json_decode($response, true);
        $this->assertTrue(isset($response['token']));

        /** Test auth ping itself */
        $browser->request(
            Request::METHOD_GET,
            '/auth/ping',
            server: ['HTTP_AUTHORIZATION' => 'Bearer ' . $response['token']]
        );
        $responsePing = (string) $browser->getResponse()->getContent();
        $this->assertJson($responsePing);
        $messageArray = json_decode($responsePing, true);
        $this->assertTrue(isset($messageArray['success']));
        $this->assertTrue($messageArray['success']);
    }
}
