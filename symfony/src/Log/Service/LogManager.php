<?php

declare(strict_types=1);

namespace App\Log\Service;

use App\Flowerpot\Repository\FlowerpotRepository;
use App\Log\Entity\Log;
use App\Log\Entity\LogType;
use App\Log\Repository\LogRepository;
use App\Log\Repository\LogTypeRepository;
use Doctrine\ORM\EntityManagerInterface;

use function is_array;
use function strval;

/** @codingStandardsIgnoreStart */
class LogManager
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private FlowerpotRepository $flowerpotRepository,
        private LogRepository $logRepository,
        private LogTypeRepository $logTypeRepository
    )
    {
    }
    /** @codingStandardsIgnoreEnd */

    /**
     * @param array<mixed> $logData
     *
     * @return array<string, mixed>
     */
    public function saveData(array $logData) : array
    {
        $unknownCodeList = [];

        foreach ($logData as $logDataItem) {
            if (! is_array($logDataItem)) {
                return ['success' => false, 'message' => 'Bad log format'];
            }

            $unknownCodes = $this->saveLog($logDataItem);
            if ($unknownCodes === null) {
                continue;
            }

            $unknownCodeList[] = $unknownCodes;
        }

        if ($unknownCodeList === []) {
            $this->entityManager->flush();

            return ['success' => true];
        }

        return ['success' => false, 'unknownCodes' => $unknownCodeList];
    }

    public function removeAllLogsByType(?LogType $logType) : void
    {
        $logs = $this->logRepository->findByLogType($logType);
        foreach ($logs as $log) {
            $this->entityManager->remove($log);
        }

        $this->entityManager->flush();
    }

    /**
     * @param array<string, mixed> $log
     *
     * @return array<mixed>|null
     */
    private function saveLog(array $log) : ?array
    {
        $logTypeCode = $this->logTypeRepository->findOneByCode($log['code'] ?? null);
        if ($logTypeCode === null && isset($log['code'])) {
            $unknownCodes['logTypeCode'] = $log['code'];
        }

        $flowerpotCode = $this->flowerpotRepository->findOneByCode($log['potCode'] ?? null);
        if ($flowerpotCode === null && isset($log['potCode'])) {
            $unknownCodes['flowerpotCode'] = $log['potCode'];
        }

        if ($flowerpotCode === null || $logTypeCode === null) {
            return $unknownCodes ?? ['code or potCode are not set'];
        }

        $newLog = new Log();
        $newLog->setValue(strval($log['value']));
        $newLog->setFlowerpot($flowerpotCode);
        $newLog->setLogType($logTypeCode);
        $this->entityManager->persist($newLog);

        return null;
    }
}
