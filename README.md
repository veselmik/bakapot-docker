# Bakapot BE

## Docker

`sudo apt-get update`

`sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common`

`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

`sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io`

### Docker without root

` sudo usermod -aG docker <user>`

## Docker compose

`sudo curl -L "https://github.com/docker/compose/releases/download/1.28.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

`sudo chmod +x /usr/local/bin/docker-compose`

## Repo install

`git clone git@gitlab.fit.cvut.cz:veselmik/bakapot-docker.git`

`cd bakapot-docker`

`make`

Then access [symfony.localhost:8081](http://symfony.localhost:8081), you should see symfony welcome page.
(url can be changed in docker-compose.yml, line 32)

### stop docker:

`make down`

### remove database:

`make clean-database`

### add user for app

`make add-user`

### list all users

`make list-users`

## Authorization

To authorize, first add user. Then send POST request to obtain the JWT token:

`curl -X POST -H "Content-Type: application/json" http://symfony.localhost:8081/login_check -d '{"email":"user@example.com","password":"test"}'`

You should now be able to visit page for logged users:

`curl --location --request GET 'bakapot.localhost:8081/auth/hello-world/greet' --header 'Authorization: Bearer {token}'`

## Code quality

To run Codesniffer

`make cs`

Run cs autofix

`make fix`

Run phpstan:

`make phpstan`
