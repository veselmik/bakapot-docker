<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\User\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const TEST_USER_EMAIL = 'user@example.com';
    public const TEST_USER_PASSWORD = 'password';

    public function __construct(private UserPasswordEncoderInterface $encoder)
    {
    }

    public function load(ObjectManager $manager) : void
    {
        $user = new User();
        $user->setEmail(self::TEST_USER_EMAIL);
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->encoder->encodePassword($user, self::TEST_USER_PASSWORD));
        $manager->persist($user);

        $manager->flush();
    }
}
