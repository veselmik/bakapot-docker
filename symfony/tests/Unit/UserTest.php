<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\User\Entity\User;
use PHPUnit\Framework\TestCase;

use function array_merge;

class UserTest extends TestCase
{
    private const USER_BASIC_ROLES = ['ROLE_USER'];

    public function testEveryUserHasRoleUser() : void
    {
        $userWithoutRole = new User();
        $this->assertSame(['ROLE_USER'], $userWithoutRole->getRoles());
    }

    public function testAddOtherRolesToUser() : void
    {
        $addedRoles = ['ROLE_LOGGER', 'ROLE_ADMIN'];
        $userWithAddedRoles = new User();
        $userWithAddedRoles->setRoles($addedRoles);
        /** two arrays equal, not dependant on the order */
        $this->assertEqualsCanonicalizing(
            array_merge(self::USER_BASIC_ROLES, $addedRoles),
            $userWithAddedRoles->getRoles()
        );
    }
}
