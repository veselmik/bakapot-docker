<?php

declare(strict_types=1);

namespace App\Log\Enum;

class LogType
{
    public const TEMP = 'TEMP';
    public const HUMID = 'HUMID';
    public const MOIST = 'MOIST';
    public const LIGHT = 'LIGHT';
    public const LIGHT_LUX = 'LIGHT_LUX';
    public const POWER = 'POWER';

    public const LOG_TYPE_UNIT = [
        self::TEMP => ['Temperature', '°C'],
        self::HUMID => ['Air humidity', '%'],
        self::MOIST => ['Soil moisture', '%'],
        self::LIGHT => ['Light level', '%'],
        self::LIGHT_LUX => ['Light level in lux', 'lx'],
        self::POWER => ['Power consumption', 'W/m'],
    ];
}
