<?php

declare(strict_types=1);

namespace App\Tests\Application;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

use function json_decode;

class PingControllerTest extends WebTestCase
{
    public function testPing() : void
    {
        $browser = static::createClient();
        $browser->request(Request::METHOD_GET, '/ping');
        $response = (string) $browser->getResponse()->getContent();

        $this->assertJson($response);
        $messageArray = json_decode($response, true);
        $this->assertTrue(isset($messageArray['success']));
        $this->assertTrue($messageArray['success']);
    }
}
