<?php

declare(strict_types=1);

namespace App\Flowerpot\Repository;

use App\Flowerpot\Entity\Flowerpot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Flowerpot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flowerpot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flowerpot[]    findAll()
 * @method Flowerpot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlowerpotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flowerpot::class);
    }

    public function findOneByCode(string $value) : ?Flowerpot
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.code = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
