<?php

declare(strict_types=1);

namespace App\Log\Repository;

use App\Log\Entity\LogType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LogType|null find($id, $lockMode = null, $lockVersion = null)
 * @method LogType|null findOneBy(array $criteria, array $orderBy = null)
 * @method LogType[]    findAll()
 * @method LogType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LogType::class);
    }

    public function findOneByCode(string $value) : ?LogType
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.code = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
