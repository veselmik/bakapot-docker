<?php

declare(strict_types=1);

namespace App\Log\Command;

use App\Log\Repository\LogTypeRepository;
use App\Log\Service\LogManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

use function assert;

class RemoveLogsCommand extends Command
{
    private const CODE_ALL = 'CODE_ALL';

    public function __construct(private LogManager $logManager, private LogTypeRepository $logTypeRepository)
    {
        parent::__construct();
    }

    protected function configure() : void
    {
        $this->setName('log:remove')
            ->setDescription('Removes logs by type.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $questionHelper = $this->getHelper('question');
        assert($questionHelper instanceof QuestionHelper);
        $questionLogType = new Question('Log type [use "!ALL" for all types]:', false);
        $code = $questionHelper->ask($input, $output, $questionLogType);
        if (! $code) {
            $output->writeln('No code provided!');

            return Command::FAILURE;
        }

        $logType = $this->logTypeRepository->findOneBy(
            ['code' => $code === self::CODE_ALL ? null : $code]
        );
        $this->logManager->removeAllLogsByType($logType);

        return Command::SUCCESS;
    }
}
