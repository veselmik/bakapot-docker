ifndef APP_ENV
APP_ENV=dev
endif

DOCKER_COMPOSE=docker-compose
DOCKER_RUN=${DOCKER_COMPOSE} exec -u $(shell id -u):$(shell id -g) php

.PHONY: build
build:
	sh shell/up.sh
	docker exec -it bakapot-docker_php_1 sh shell/db.sh
	sudo chown -R `whoami`:www-data .

.PHONY: clean-database
clean-database:
	${DOCKER_COMPOSE} down
	docker volume rm bakapot-docker_db_app

.PHONY: fix
fix:
	${DOCKER_RUN} vendor/bin/phpcbf

.PHONY: cs
cs:
	${DOCKER_RUN} vendor/bin/phpcs

.PHONY: phpstan
phpstan:
	${DOCKER_RUN} vendor/bin/phpstan analyse

.PHONY: down
down:
	${DOCKER_COMPOSE} down

.PHONY: add-user
add-user:
	${DOCKER_RUN} bin/console app:user:create

.PHONY: list-users
list-users:
	${DOCKER_RUN} bin/console app:user:list

.PHONY: log-types-dump
log-types-dump:
	${DOCKER_RUN} bin/console log-types:dump

.PHONY: list-logs
list-logs:
	${DOCKER_RUN} bin/console log:list

.PHONY: remove-logs
remove-logs:
	${DOCKER_RUN} bin/console log:remove

.PHONY: fix-rights
fix-rights:
	sudo chown -R `whoami`:www-data .

.PHONY: add-pot
add-pot:
	${DOCKER_RUN} bin/console flowerpot:add
