<?php

declare(strict_types=1);

namespace App\Log\Repository;

use App\Flowerpot\Repository\FlowerpotRepository;
use App\Log\Entity\Log;
use App\Log\Entity\LogType;
use DateInterval;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Throwable;

/**
 * @method Log|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log[]    findAll()
 * @method Log[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry,
        private LogTypeRepository $logTypeRepository,
        private FlowerpotRepository $flowerpotRepository
    ) {
        parent::__construct($registry, Log::class);
    }

    /** @return array<Log> */
    public function findByLogType(?LogType $logType, ?int $limit = null) : array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('l')
            ->from(Log::class, 'l')
            ->orderBy('l.created', 'DESC');
        if ($logType !== null) {
            $queryBuilder = $queryBuilder->where('l.logType = :logType')
                ->setParameter('logType', $logType);
        }

        if ($limit !== null) {
            $queryBuilder->setMaxResults($limit);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /** @return array<Log> */
    public function getByLogTypeAndPot(
        string $logTypeCode,
        string $flowerpotCode,
        string $startDate = 'now',
        ?string $endDate = null
    ) : array {
        $flowerpot = $this->flowerpotRepository->findOneByCode($flowerpotCode);
        $logType = $this->logTypeRepository->findOneByCode($logTypeCode);
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('l')
            ->from(Log::class, 'l')
            ->orderBy('l.created', 'DESC')
            ->where('l.flowerpot = :flowerpot')
            ->andWhere('l.logType = :logType')
            ->setParameter('flowerpot', $flowerpot)
            ->setParameter('logType', $logType)
            ->orderBy('l.created', 'ASC');

        try {
            $oneDayInterval = (new DateInterval('P1D'));
            $startDateDT = new DateTimeImmutable($startDate);
            $endDateDT = $endDate !== null
                ? new DateTimeImmutable($endDate)
                : $startDateDT->add($oneDayInterval);

            $queryBuilder->andWhere('l.created >= :startDate')
                ->andWhere('l.created <= :endDate')
                ->setParameter('startDate', $startDateDT)
                ->setParameter('endDate', $endDateDT);
        } catch (Throwable $e) {
            $queryBuilder->setMaxResults(1440);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
