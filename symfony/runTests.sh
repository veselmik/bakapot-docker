#!/bin/bash
TEST_DB_LOCK_FILE=".test-db-created"
if [ ! -f "$TEST_DB_LOCK_FILE" ]; then
  bin/console --env=test doctrine:database:create
  bin/console --env=test doctrine:schema:create
  touch "$TEST_DB_LOCK_FILE"
fi

bin/console -n --env=test doctrine:fixtures:load
vendor/bin/phpunit