<?php

declare(strict_types=1);

namespace App\Ping\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AuthPingController
{
    #[Route('/auth/ping', name: 'auth.ping')]
    public function action() : JsonResponse
    {
        return new JsonResponse(
            ['success' => true]
        );
    }
}
