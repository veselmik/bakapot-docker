<?php

declare(strict_types=1);

namespace App\Ping\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PingController
{
    #[Route('/ping', name: 'ping')]
    public function action() : JsonResponse
    {
        return new JsonResponse(
            ['success' => true]
        );
    }
}
