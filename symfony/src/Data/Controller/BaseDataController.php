<?php

declare(strict_types=1);

namespace App\Data\Controller;

use App\Log\Entity\Log;
use App\Log\Payload\Log as LogPayload;

use function array_map;
use function count;
use function floatval;
use function floor;

abstract class BaseDataController
{
    private const CHART_OPTIMAL_VALUES_COUNT = 300;

    /**
     * @param list<Log> $logs
     *
     * @return list<LogPayload>
     */
    protected function serializePayload(array $logs) : array
    {
        return array_map(
            static function (Log $log) {
                return new LogPayload(
                    $log->getCreated()->format('c'),
                    $log->getCreated()->getTimestamp(),
                    $log->getValue()
                );
            },
            $logs
        );
    }

    /**
     * @param list<Log> $logs
     *
     * @return array<string, mixed>
     */
    protected function getChartData(array $logs, bool $convertValueToFloat = true) : array
    {
        $logCount = count($logs);
        $everyNth = (int) floor($logCount / self::CHART_OPTIMAL_VALUES_COUNT);
        $currentIndex = 0;
        $returnArray = [
            'dates' => [],
            'timestamps' => [],
            'values' => [],
        ];
        foreach ($logs as $log) {
            /** filter too many values */
            if ($currentIndex < $logCount && ($currentIndex % $everyNth === 0) || ($currentIndex === $logCount - 1)) {
                $returnArray['dates'][] = $log->getCreated()->format('Y-m-d H:i');
                $returnArray['timestamps'][] = $log->getCreated()->getTimestamp();
                $returnArray['values'][] = $convertValueToFloat ? floatval($log->getValue()) : $log->getValue();
            }

            $currentIndex++;
        }

        return $returnArray;
    }
}
