<?php

declare(strict_types=1);

namespace App\Log\Command;

use App\Log\Entity\LogType;
use App\Log\Enum\LogType as LogTypeEnum;
use App\Log\Repository\LogTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LogTypesCommand extends Command
{
    private EntityManagerInterface $em;
    private LogTypeRepository $repo;

    public function __construct(EntityManagerInterface $em, LogTypeRepository $repo)
    {
        $this->em = $em;
        $this->repo = $repo;

        parent::__construct();
    }

    protected function configure() : void
    {
        $this->setName('log-types:dump');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $count = $this->insertLogTypes();
        $output->writeln('INSERTED ' . $count . ' log types');

        return Command::SUCCESS;
    }

    private function insertLogTypes() : int
    {
        $count = 0;
        foreach (LogTypeEnum::LOG_TYPE_UNIT as $logTypeCode => $logTypeProps) {
            $logType = $this->repo->findOneByCode($logTypeCode) ?? new LogType();
            $logType->setCode($logTypeCode);
            $logType->setDescription($logTypeProps[0]);
            $logType->setUnit($logTypeProps[1]);
            $this->em->persist($logType);
            $count++;
        }

        $this->em->flush();

        return $count;
    }
}
