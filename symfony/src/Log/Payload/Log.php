<?php

declare(strict_types=1);

namespace App\Log\Payload;

class Log
{
    public function __construct(public string $created, public int $createdTs, public string $value)
    {
    }
}
