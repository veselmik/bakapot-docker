<?php

declare(strict_types=1);

namespace App\Log\Entity;

use App\Flowerpot\Entity\Flowerpot;
use App\Log\Repository\LogRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity(repositoryClass=LogRepository::class) */
class Log
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /** @ORM\Column(type="datetime_immutable", options={"default": "CURRENT_TIMESTAMP"}) */
    private DateTimeImmutable $created;

    /** @ORM\Column(type="string", length=20, nullable=true) */
    private ?string $value;

    /**
     * @ORM\ManyToOne(targetEntity=LogType::class, inversedBy="logs")
     * @ORM\JoinColumn(nullable=false)
     */
    private LogType $logType;

    /**
     * @ORM\ManyToOne(targetEntity=Flowerpot::class, inversedBy="logs")
     * @ORM\JoinColumn(nullable=false)
     */
    private Flowerpot $flowerpot;

    public function __construct()
    {
        $this->created = new DateTimeImmutable();
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getCreated() : DateTimeImmutable
    {
        return $this->created;
    }

    public function setCreated(DateTimeImmutable $created) : self
    {
        $this->created = $created;

        return $this;
    }

    public function getValue() : string
    {
        return $this->value ?? 'N/A';
    }

    public function setValue(string $value) : self
    {
        $this->value = $value;

        return $this;
    }

    public function getLogType() : LogType
    {
        return $this->logType;
    }

    public function setLogType(LogType $logType) : self
    {
        $this->logType = $logType;

        return $this;
    }

    public function getFlowerpot() : Flowerpot
    {
        return $this->flowerpot;
    }

    public function setFlowerpot(Flowerpot $flowerpot) : self
    {
        $this->flowerpot = $flowerpot;

        return $this;
    }
}
