<?php

declare(strict_types=1);

namespace App\Flowerpot\Service;

use App\Core\Service\EntityManagerConstructor;
use App\Flowerpot\Entity\Flowerpot;
use App\Flowerpot\Repository\FlowerpotRepository;
use Doctrine\ORM\EntityManagerInterface;

class FlowerpotManager extends EntityManagerConstructor
{
    private FlowerpotRepository $flowerpotRepository;

    public function __construct(EntityManagerInterface $entityManager, FlowerpotRepository $flowerpotRepository)
    {
        parent::__construct($entityManager);

        $this->flowerpotRepository = $flowerpotRepository;
    }

    public function create(string $description, string $code) : bool
    {
        if ($this->flowerpotRepository->findBy(['code' => $code])) {
            return false;
        }

        $newUser = new Flowerpot();
        $newUser->setDescription($description);
        $newUser->setCode($code);
        $this->em->persist($newUser);
        $this->em->flush();

        return true;
    }
}
