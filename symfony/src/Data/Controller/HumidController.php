<?php

declare(strict_types=1);

namespace App\Data\Controller;

use App\Log\Enum\LogType;
use App\Log\Repository\LogRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HumidController extends BaseDataController
{
    public function __construct(private LogRepository $logRepository)
    {
    }

    #[Route('/data/humid/chart', name: 'data.humid.chart', methods: ['POST'])]
    public function actionChartData(Request $request) : JsonResponse
    {
        $logs = $this->logRepository->getByLogTypeAndPot(
            LogType::HUMID,
            $request->get('potCode'),
            $request->get('startDate'),
            $request->get('endDate')
        );

        return new JsonResponse($this->getChartData($logs));
    }
}
