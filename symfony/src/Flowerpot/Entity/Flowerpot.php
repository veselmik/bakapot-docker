<?php

declare(strict_types=1);

namespace App\Flowerpot\Entity;

use App\Log\Entity\Log;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity(repositoryClass=FlowerpotRepository::class) */
class Flowerpot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /** @ORM\Column(type="string") */
    private string $description;

    /** @ORM\Column(type="string", unique=true) */
    private string $code;

    /**
     * @ORM\OneToMany(targetEntity=Log::class, mappedBy="flowerpot")
     *
     * @var Collection<Log>
     */
    private Collection $logs;

    public function getId() : int
    {
        return $this->id;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function setCode(string $code) : void
    {
        $this->code = $code;
    }
}
