<?php

declare(strict_types=1);

namespace App\Log\Command;

use App\Log\Entity\Log;
use App\Log\Repository\LogRepository;
use App\Log\Repository\LogTypeRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

use function array_map;
use function assert;

class ListLogsCommand extends Command
{
    public function __construct(private LogRepository $logRepository, private LogTypeRepository $logTypeRepository)
    {
        parent::__construct();
    }

    protected function configure() : void
    {
        $this->setName('log:list')
            ->setDescription('Lists logs (with optional type parameter).');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $questionHelper = $this->getHelper('question');
        assert($questionHelper instanceof QuestionHelper);
        $questionLogType = new Question('Log type [leave blank for all types]:', null);
        $questionLimit = new Question('How many last rows [leave blank for 10]:', 10);
        $logType = $this->logTypeRepository->findOneBy(
            ['code' => $questionHelper->ask($input, $output, $questionLogType)]
        );
        $table = new Table($output);
        $table
            ->setHeaders(['Logged', 'Code', 'Flowerpot code', 'Value']);
        $table->setRows(
            array_map(
                static function (Log $log) : array {
                    return [
                        $log->getCreated()->format('Y-m-d H:i:s'),
                        $log->getLogType()->getCode(),
                        $log->getFlowerpot()->getCode(),
                        $log->getValue(),
                    ];
                },
                $this->logRepository->findByLogType(
                    $logType,
                    (int) $questionHelper->ask($input, $output, $questionLimit)
                )
            )
        );

        $table->render();

        return Command::SUCCESS;
    }
}
