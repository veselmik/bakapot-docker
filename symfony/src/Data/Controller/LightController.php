<?php

declare(strict_types=1);

namespace App\Data\Controller;

use App\Log\Enum\LogType;
use App\Log\Repository\LogRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LightController extends BaseDataController
{
    public function __construct(private LogRepository $logRepository)
    {
    }

    #[Route('/data/light/chart', name: 'data.light.chart', methods: ['POST'])]
    public function actionChartData(Request $request) : JsonResponse
    {
        $logs = $this->logRepository->getByLogTypeAndPot(
            LogType::LIGHT,
            $request->get('potCode'),
            $request->get('startDate'),
            $request->get('endDate')
        );

        return new JsonResponse($this->getChartData($logs));
    }
}
