<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use App\User\Entity\User;
use App\User\Repository\UserRepository;
use App\User\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use function assert;

class UserManagerTest extends KernelTestCase
{
    private const NEW_USER_EMAIL = 'new_user@example.com';
    private const NEW_USER_PASSWORD = 'password';
    private const NEW_USER_ROLE = 'ROLE_LOGGER';

    public function testCreateUser() : void
    {
        self::bootKernel();
        $container = self::$container;
        $userManager = $container->get(UserManager::class);
        assert($userManager instanceof UserManager);
        $userManager->create(self::NEW_USER_EMAIL, self::NEW_USER_PASSWORD, self::NEW_USER_ROLE);

        $userRepository = $container->get(UserRepository::class);
        assert($userRepository instanceof UserRepository);
        $newUser = $userRepository->findOneBy(['email' => self::NEW_USER_EMAIL]);
        $this->assertNotNull($newUser);
        $this->assertInstanceOf(User::class, $newUser);
    }
}
