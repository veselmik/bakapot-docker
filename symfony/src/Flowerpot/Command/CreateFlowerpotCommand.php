<?php

declare(strict_types=1);

namespace App\Flowerpot\Command;

use App\Flowerpot\Service\FlowerpotManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateFlowerpotCommand extends Command
{
    private ?QuestionHelper $questionHelper;

    public function __construct(private FlowerpotManager $flowerpotManager)
    {
        $this->questionHelper = null;

        parent::__construct();
    }

    protected function configure() : void
    {
        $this->setName('flowerpot:add');
        $this
            ->setDescription('Creates a new flowerpot.')
            ->setHelp('This command allows you to create a flowerpot...');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $this->questionHelper = $this->getHelper('question');
        $questionDescription = new Question('Flowerpot desciption:', false);
        $questionCode = new Question('Flowerpot code:', false);
        $description = $this->questionHelper->ask($input, $output, $questionDescription);
        $code = $this->questionHelper->ask($input, $output, $questionCode);

        if (! $description || ! $code) {
            $output->writeln('<error>Failed: Enter valid credentials</error>');

            return Command::FAILURE;
        }

        if (
            ! $this->flowerpotManager->create(
                $description,
                $code
            )
        ) {
            $output->writeln('<error>Failed: Flowerpot with entered code already exists</error>');

            return Command::FAILURE;
        }

        $output->writeln('<info>Success: Flowerpot with code ' . $code . ' created</info>');

        return Command::SUCCESS;
    }
}
