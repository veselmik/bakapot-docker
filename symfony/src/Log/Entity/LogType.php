<?php

declare(strict_types=1);

namespace App\Log\Entity;

use App\Log\Repository\LogTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity(repositoryClass=LogTypeRepository::class) */
class LogType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /** @ORM\Column(type="string", length=10, unique=true) */
    private string $code;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private string $description;

    /**
     * @ORM\OneToMany(targetEntity=Log::class, mappedBy="logType")
     *
     * @var Collection<Log>
     */
    private Collection $logs;

    /** @ORM\Column(type="string", length=20, nullable=true) */
    private string $unit;

    public function __construct()
    {
        $this->logs = new ArrayCollection();
    }

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getCode() : ?string
    {
        return $this->code;
    }

    public function setCode(string $code) : self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription() : ?string
    {
        return $this->description;
    }

    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    public function getUnit() : ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit) : self
    {
        $this->unit = $unit;

        return $this;
    }
}
